import Vue from 'vue'
import AppCondition from '~/components/fields/AppConditionComponent.vue'
import AppRange from '~/components/fields/AppRangeComponent.vue'
import AppSelect from '~/components/fields/AppSelectComponent.vue'

Vue.component('app-condition', AppCondition)
Vue.component('app-range', AppRange)
Vue.component('app-select', AppSelect)
